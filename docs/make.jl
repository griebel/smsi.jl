using SMSI
using Documenter
using LightGraphs
using Colors
using GraphPlot
using Compose

DocMeta.setdocmeta!(SMSI, :DocTestSetup, :(using SMSI); recursive=true)

makedocs(;
    modules=[SMSI],
    authors="Oliver Griebel",
    repo="https://gitlab.com/griebel/SMSI.jl/blob/{commit}{path}#{line}",
    sitename="SMSI.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://griebel.gitlab.io/SMSI.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "Chapter 2" => [
            "Amdahls Law" => "chapter2/amdahls_law.md",
            "Gustafsons Law" => "chapter2/gustafsons_law.md",
            "Kernighan-Lin" => "chapter2/kernighan_lin.md",
            "Simulated Annealing" => "chapter2/simulated_annealing.md",
        ],
        "Functions" => "functions.md",
    ],
)
