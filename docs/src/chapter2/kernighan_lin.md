```@meta
DocTestSetup = quote
    import Pkg
    Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add("Compose")
end
```

# Kernighan Lin Algorithm
## Problem
### Partitions

```jldoctest kernighan_lin
julia> using SMSI

julia> V_1 = [1, 2, 3, 4, 14, 15, 16, 17, 18]
9-element Vector{Int64}:
  1
  2
  3
  4
 14
 15
 16
 17
 18

julia> V_2 = [5, 6, 7, 8, 9, 10, 11, 12, 13]
9-element Vector{Int64}:
  5
  6
  7
  8
  9
 10
 11
 12
 13
```

### Cut size

```jldoctest kernighan_lin
julia> c_0 = 10
10
```

### Graph

```jldoctest kernighan_lin
julia> using LightGraphs

julia> S = SimpleGraph(18)
{18, 0} undirected simple Int64 graph

julia> add_edge!(S, 1, 2)
true

julia> add_edge!(S, 1, 3)
true

julia> add_edge!(S, 1, 4)
true

julia> add_edge!(S, 1, 5)
true

julia> add_edge!(S, 2, 3)
true

julia> add_edge!(S, 2, 4)
true

julia> add_edge!(S, 2, 5)
true

julia> add_edge!(S, 3, 4)
true

julia> add_edge!(S, 3, 6)
true

julia> add_edge!(S, 3, 10)
true

julia> add_edge!(S, 4, 6)
true

julia> add_edge!(S, 4, 11)
true

julia> add_edge!(S, 5, 7)
true

julia> add_edge!(S, 5, 8)
true

julia> add_edge!(S, 5, 9)
true

julia> add_edge!(S, 6, 7)
true

julia> add_edge!(S, 6, 8)
true

julia> add_edge!(S, 6, 9)
true

julia> add_edge!(S, 10, 11)
true

julia> add_edge!(S, 10, 12)
true

julia> add_edge!(S, 10, 13)
true

julia> add_edge!(S, 10, 14)
true

julia> add_edge!(S, 11, 12)
true

julia> add_edge!(S, 11, 13)
true

julia> add_edge!(S, 11, 14)
true

julia> add_edge!(S, 12, 13)
true

julia> add_edge!(S, 12, 15)
true

julia> add_edge!(S, 13, 15)
true

julia> add_edge!(S, 14, 16)
true

julia> add_edge!(S, 14, 17)
true

julia> add_edge!(S, 14, 18)
true

julia> add_edge!(S, 15, 16)
true

julia> add_edge!(S, 15, 17)
true

julia> add_edge!(S, 15, 18)
true
```

### Plot

```jldoctest kernighan_lin
julia> using Colors

julia> using GraphPlot

julia> using Compose

julia> nodelabel_0 = 1:nv(S)
1:18

julia> nodefillc_0 = fill(colorant"lightblue", nv(S)); nothing # hide

julia> nodefillc_0[V_2] .= colorant"lightyellow"; nothing # hide

julia> p_0 = gplot(S, nodefillc=nodefillc_0, nodelabel=nodelabel_0); nothing # hide

julia> draw(SVG("build/chapter2/kernighan_lin_0.svg"), p_0); nothing # hide
```

![](kernighan_lin_0.svg)

## Optimization
### Iteration 1

```jldoctest kernighan_lin
julia> (c_1, V_1_1, V_2_1) = kernighan_lin(S, c_0, V_1, V_2)
(9, [8, 9, 5, 7, 14, 15, 16, 17, 18], [3, 6, 4, 1, 2, 10, 11, 12, 13])

julia> nodelabel_1 = 1:nv(S)
1:18

julia> nodefillc_1 = fill(colorant"lightblue", 18); nothing # hide

julia> nodefillc_1[V_2_1] .= colorant"lightyellow"; nothing # hide

julia> p_1 = gplot(S, nodefillc=nodefillc_1, nodelabel=nodelabel_1); nothing # hide

julia> draw(SVG("build/chapter2/kernighan_lin_1.svg"), p_1); nothing # hide
```

![](kernighan_lin_1.svg)

### Plot

```jldoctest kernighan_lin
julia> (c_2, V_1_2, V_2_2) = kernighan_lin(S, c_1, V_1_1, V_2_1)
(9, [8, 9, 5, 7, 14, 15, 16, 17, 18], [3, 6, 4, 1, 2, 10, 11, 12, 13])

julia> nodelabel_2 = 1:nv(S)
1:18

julia> nodefillc_2 = fill(colorant"lightblue", 18); nothing # hide

julia> nodefillc_2[V_2_2] .= colorant"lightyellow"; nothing # hide

julia> p_2 = gplot(S, nodefillc=nodefillc_2, nodelabel=nodelabel_2); nothing # hide

julia> draw(SVG("build/chapter2/kernighan_lin_2.svg"), p_2); nothing # hide
```

![](kernighan_lin_2.svg)

```@meta
DocTestSetup = nothing
```