```@meta
DocTestSetup = quote
    import Pkg
    Pkg.add("CairoMakie")
end
```

# Simulated Annealing
## Initial path

```jldoctest simulated_annealing
julia> using SMSI

julia> using CairoMakie

julia> path = [
               (200, 40),
               (60, 80),
               (100, 40),
               (180, 60),
               (20, 20),
               (180, 200),
               (120, 80),
               (60, 200),
               (20, 40),
               (200, 160),
               (140, 140),
               (60, 20),
               (160, 20),
               (140, 180),
               (180, 100),
               (20, 160),
               (80, 180),
               (40, 120),
           ]
18-element Vector{Tuple{Int64, Int64}}:
 (200, 40)
 (60, 80)
 (100, 40)
 (180, 60)
 (20, 20)
 (180, 200)
 (120, 80)
 (60, 200)
 (20, 40)
 (200, 160)
 (140, 140)
 (60, 20)
 (160, 20)
 (140, 180)
 (180, 100)
 (20, 160)
 (80, 180)
 (40, 120)

julia> fig_0 = Figure(); nothing # hide

julia> ax_0 = Axis(fig_0[1, 1]); nothing # hide

julia> scatter!(ax_0, path, color=:red); nothing # hide

julia> lines!(ax_0, path, color=:blue); nothing # hide

julia> lines!(ax_0, [path[end], path[1]], color=:blue); nothing # hide

julia> save("build/chapter2/simulated_annealing_0.svg", fig_0); nothing # hide

julia> cost = distance(path)
2383.251998352831
```

![](simulated_annealing_0.svg)

## Optimize

```jldoctest simulated_annealing
julia> opt_cost, opt_path = simulated_annealing(path, 0.000001, 0.99, 2500); nothing # hide due to randomnes

julia> fig_1 = Figure(); nothing # hide

julia> ax_1 = Axis(fig_1[1, 1]); nothing # hide

julia> scatter!(ax_1, opt_path, color=:red); nothing # hide

julia> lines!(ax_1, opt_path, color=:blue); nothing # hide

julia> lines!(ax_1, [opt_path[end], opt_path[1]], color=:blue); nothing # hide

julia> save("build/chapter2/simulated_annealing_1.svg", fig_1); nothing # hide
```

![](simulated_annealing_1.svg)

```@meta
DocTestSetup = nothing
```