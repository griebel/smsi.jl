```@meta
DocTestSetup = quote
    import Pkg
    Pkg.add("CairoMakie")
end
```

# Amdahls Law
## Define constants

```jldoctest amdahls_law
julia> using SMSI

julia> t_s = 1000
1000

julia> t_p = 9000
9000

julia> f = amdahl_fraction(t_s, t_p)
0.1
```

## Check speedup

```jldoctest amdahls_law
julia> N = 4
4

julia> amdahl_speedup(f, N)
3.0769230769230766
```

## Plot

```jldoctest amdahls_law
julia> using CairoMakie

julia> N_min = 1
1

julia> N_max = 512
512

julia> xs = LinRange{Integer}(N_min, N_max, 512)
512-element LinRange{Integer}:
 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,…,504,505,506,507,508,509,510,511,512

julia> ys = [amdahl_speedup(f, x) for x in xs]
512-element Vector{Float64}:
 1.0
 1.8181818181818181
 2.5
 3.0769230769230766
 3.571428571428571
 4.0
 4.375
 4.705882352941176
 5.0
 5.2631578947368425
 ⋮
 9.824561403508772
 9.824902723735407
 9.825242718446601
 9.825581395348836
 9.825918762088973
 9.826254826254825
 9.826589595375722
 9.826923076923077
 9.82725527831094

julia> f = lines(xs, ys, axis=(type=Axis,xlabel="N",ylabel="S"))
FigureAxisPlot()

julia> save("build/chapter2/amdahls_law.svg", f); nothing # hide
```

![](amdahls_law.svg)

```@meta
DocTestSetup = nothing
```