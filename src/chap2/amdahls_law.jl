"""
    amdahl_fraction(t_s::Integer, t_p::Integer) -> AbstractFloat

Amdahl's Law

Fraction of non-parallel code

# Arguments
- `t_s::Integer`: non-parallelizable (serial) part of the code
- `t_p::Integer`: parallelizable part of the code

# Returns
- `AbstractFloat`: fraction of non-parallel code
"""
function amdahl_fraction(t_s::Integer, t_p::Integer)::AbstractFloat
    f = (t_s / (t_s + t_p))

    return f
end


"""
    amdahl_speedup(f::AbstractFloat, N::Integer) -> AbstractFloat

Amdahl's Law

Considers a fixed size problem.

# Arguments
- `f::AbstractFloat`: fraction of non-parallel code
- `N::Integer`: number of processor Considers

# Returns
- `AbstractFloat`: speedup
"""
function amdahl_speedup(f::AbstractFloat, N::Integer)::AbstractFloat
    # speedup
    s = 1 / (f + (1 - f) / N)

    return s
end
