"""
    edge_cost(S::SimpleGraph, v_i::T, V::Vector{T}) -> T where {T<:Integer}

Calculates the cost of the connection of the vertex `v_i` to the set of vertices `V`

# Arguments
- `S::SimpleGraph`: Graph
- `v_i::T`: considered vertex
- `V::Vector{T}`: set of vertices

# Returns
- `T`: connection cost
"""
function edge_cost(S::SimpleGraph, v_i::T, V::Vector{T})::Integer where {T<:Integer}
    omega = weights(S)

    c = 0
    for v_j in V
        if (has_edge(S, v_i, v_j))
            c += omega[v_i, v_j]
        end
    end

    return c
end


"""
    gain(
        S::SimpleGraph,
        v_i::Integer,
        V_1::Vector{Int},
        V_2::Vector{Int}
    ) -> T where {T<:Integer}

Difference of cut size, if vertex is moved from its set of vertices to the other

# Arguments
- `S::SimpleGraph`: Graph
- `v_i::T`: vertex
- `V_1::Vector{T}`: set of vertices 1
- `V_2::Vector{T}`: set of vertices 2

# Returns
- `T`: cut size difference
"""
function gain(
    S::SimpleGraph,
    v_i::T,
    V_1::Vector{T},
    V_2::Vector{T}
)::T where {T<:Integer}
    v_i_in_V_1 = false
    for v in V_1
        if (v == v_i)
            v_i_in_V_1 = true
        end
    end

    if (v_i_in_V_1)
        I = edge_cost(S, v_i, V_1)
        E = edge_cost(S, v_i, V_2)
    else
        I = edge_cost(S, v_i, V_2)
        E = edge_cost(S, v_i, V_1)
    end

    g = E - I

    return g
end


"""
    vertex_in_partition(v_i::T, V::Vector{T}) -> Bool where {T<:Integer}

Checks if the vertex `v_i` is in the set of vertices `V`.

# Arguments
- `v_i::T`: vertex
- `V::Vector{T}`: set of vertices

# Returns
- `Bool`: true, if `v_i` is an element of `V`
"""
function vertex_in_partition(v_i::T, V::Vector{T})::Bool where {T<:Integer}
    for v in V
        if (v == v_i)
            return true
        end
    end

    return false
end


"""
    kernighan_lin(
        S::SimpleGraph,
        c::T,
        A::Vector{T},
        B::Vector{T},
    ) -> Tuple{T,Vector{T},Vector{T}} where {T<:Integer}

Kernighan Lin Algorithm

Original paper on [IEEExplore](https://ieeexplore.ieee.org/document/6771089)

# Arguments
- `S::SimpleGraph`: finite state set
- `c::T`: cost function
- `A::Vector{T}`: initial partition A
- `B::Vector{T}`: initial partition B

# Returns
- `Tuple{T,Vector{T},Vector{T}}`:
    - `T`: cut size
    - `Vector{T}`: optimized partition A
    - `Vector{T}`: optimized partition B
"""
function kernighan_lin(
    S::SimpleGraph,
    c::Integer,
    A::Vector{T},
    B::Vector{T},
)::Tuple{T,Vector{T},Vector{T}} where {T<:Integer}
    # Variables
    n = nv(S)
    omega = copy(weights(S))
    pairs = zeros(T, (div(n, 2), 2))
    locked = zeros(Bool, n)
    costs = zeros(T, div(n, 2) + 1)
    D = zeros(T, n)

    # Compute weights
    for i in 1:n
        for j in 1:n
            if (!has_edge(S, i, j))
                omega[i, j] = 0
            end
        end
    end

    # Compute D values
    for i in 1:n
        D[i] = gain(S, i, A, B)
    end

    # Initialization
    costs[1] = c
    best_cost = costs[1]
    best_change = 0

    for s in 1:div(n, 2)
        costs[s+1] = typemax(Int64)

        # Search for best pair to be exchanged
        for i in A
            if (locked[i])
                continue
            end

            for j in B
                if (locked[j])
                    continue
                end

                if (2 * omega[i, j] - D[i] - D[j] < costs[s+1])
                    pairs[s, 1] = i
                    pairs[s, 2] = j
                    costs[s+1] = 2 * omega[i, j] - D[i] - D[j]
                end
            end
        end

        i_min = pairs[s, 1]
        j_min = pairs[s, 2]

        locked[i_min] = true
        locked[j_min] = true

        # update gains
        for i in 1:n
            if (locked[i])
                continue
            end

            if (vertex_in_partition(i, A))
                D[i] = D[i] - 2 * omega[i, j_min] + 2 * omega[i, i_min]
            else
                D[i] = D[i] - 2 * omega[i, i_min] + 2 * omega[i, j_min]
            end
        end

        # update cut size
        costs[s+1] = costs[s] + costs[s+1]

        # check for minimum cut size
        if (costs[s+1] < best_cost)
            best_change = s
            best_cost = costs[s+1]
        end
    end

    # Use copy of Vectors
    V_1 = copy(A)
    V_2 = copy(B)
    for s in 1:best_change
        for i in 1:length(V_1)
            if (V_1[i] == pairs[s, 1])
                V_1[i] = pairs[s, 2]
            end
        end

        for i in 1:length(V_2)
            if (V_2[i] == pairs[s, 2])
                V_2[i] = pairs[s, 1]
            end
        end
    end

    return (best_cost, V_1, V_2)
end
