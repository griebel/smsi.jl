"""
    simulated_annealing(
        path::Vector{Tuple{Int,Int}},
        T_min::AbstractFloat,
        alpha::AbstractFloat,
        iter::Integer,
    ) -> Tuple{AbstractFloat,Vector{Tuple{Int,Int}}}

Simulated Annealing Algorithm

Original paper on [Science](https://science.sciencemag.org/content/220/4598/671)

# Arguments
- `path::Vector{Tuple{Int,Int}}`: locations in order to be reached
- `T_min::AbstractFloat`: minimum temperature as stopping criterion (0.0 < T_min < 1.0)
- `alpha::AbstractFloat`: temperature degradation factor (0.0 < alpha < 1.0)
- `iter::Integer`: number of iterations before next temperature decrease (0 < iter)

# Returns
- `Tuple{AbstractFloat,Vector{Tuple{Int,Int}}}`:
    - `AbstractFloat`: resulting cost
    - `Vector{Tuple{Int,Int}}`: resulting path
"""
function simulated_annealing(
    path::Vector{Tuple{Int,Int}},
    T_min::AbstractFloat,
    alpha::AbstractFloat,
    iter::Integer,
)::Tuple{AbstractFloat,Vector{Tuple{Int,Int}}}
    @assert 0.0 < T_min < 1.0 "Variable needs to be within: 0.0 < T_min < 1.0"
    @assert 0.0 < alpha < 1.0 "Variable needs to be within: 0.0 < alpha < 1.0"
    @assert 0 < iter "Variable needs to be within: 0 < iter"

    T = 1.0

    sol = path
    cost = distance(sol)
    while (T > T_min)
        i = 1
        while (i <= iter)
            new_sol = random_exchange_pairs(sol)
            new_cost = distance(new_sol)
            delta_cost = new_cost - cost
            if (rand() <= min(1, exp(-delta_cost / T)))
                sol = new_sol
                cost = new_cost
            end
            i += 1
        end
        T *= alpha
    end

    return (cost, sol)
end
