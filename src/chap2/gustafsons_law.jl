"""
    gustafson_speedup(t_s::Integer, t_p::Integer, N::Integer) -> AbstractFloat

Gustafson's Law

Considered that a problem size scales.

# Arguments
- `t_s::Integer`: non-parallelizable (serial) part of the code
- `t_p::Integer`: parallelizable part of the code
- `N::Integer`: number of processor Considers

# Returns
- `AbstractFloat`: speedup
"""
function gustafson_speedup(t_s::Integer, t_p::Integer, N::Integer)::AbstractFloat
    # speedup
    s = (t_s + t_p) / (t_s + (t_p / N))

    return s
end
