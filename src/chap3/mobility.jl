"""
    mobility(dfg::MetaDiGraph, delta::Dict{Symbol,T}) -> Vector{T} where {T<:Integer}

Priority function for mobility yield

# Arguments
- `dfg::MetaDiGraph`: graph of operations including `:type`
- `delta::Dict{Symbol,Int}`: delay per operation `:type`

# Returns
- `Vector{Int}`: priorities according to operation
"""
function mobility(dfg::MetaDiGraph, delta::Dict{Symbol,T})::Vector{T} where {T<:Integer}
    alap = alap_scheduling(dfg, delta)
    asap = asap_scheduling(dfg, delta)

    priority = zeros(Int, nv(dfg))
    for o_i in 1:nv(dfg)
        priority[o_i] = alap[o_i] - asap[o_i]
    end

    return priority
end
