"""
    alap_scheduling(
        dfg::MetaDiGraph,
        delta::Dict{Symbol,T}
    ) -> Vector{T} where {T<:Integer}

ALAP Scheduling

# Arguments
- `dfg::MetaDiGraph`: graph of operations including `:type`
- `delta::Dict{Symbol,T}`: delay per operation `:type`

# Returns
- `Vector{T}`: clock cycle of execution of each operation
"""
function alap_scheduling(
    dfg::MetaDiGraph,
    delta::Dict{Symbol,T}
)::Vector{T} where {T<:Integer}
    # get in degree
    out_degree = zeros(T, nv(dfg))
    for i in 1:nv(dfg)
        out_degree[i] = outdegree(dfg, i)
    end

    # Define source set
    stack = Stack{T}()
    for i in 1:nv(dfg)
        if (out_degree[i] == 0)
            push!(stack, i)
        end
    end

    # set outputs
    phi = ones(T, nv(dfg))
    while (!isempty(stack))
        o_i = pop!(stack)
        for o_j in inneighbors(dfg, o_i)
            type = get_prop(dfg, o_i, :type)
            phi[o_j] = max(phi[o_i] + delta[type], phi[o_j])
            out_degree[o_j] -= 1
            if (out_degree[o_j] == 0)
                push!(stack, o_j)
            end
        end
    end

    # reverse order
    last = maximum(phi)
    slowest = 0
    for (k, v) in delta
        if slowest < v
            slowest = v
        end
    end
    for o_i in 1:nv(dfg)
        type = get_prop(dfg, o_i, :type)
        phi[o_i] = last - phi[o_i] + slowest - delta[type] + 1
    end

    return phi
end
