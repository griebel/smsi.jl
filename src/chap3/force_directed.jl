"""
    force_directed_scheduling(
        dfg::MetaDiGraph,
        delta::Dict{Symbol,T}
    ) -> Vector{T} where {T<:Integer}

Force Directed Scheduling

# Arguments

# Returns
"""
function force_directed_scheduling(
    dfg::MetaDiGraph,
    delta::Dict{Symbol,T}
)::Vector{T} where {T<:Integer}
    phi = zeros(T, nv(dfg))
    done = false

    while (!done)
        ## Step 1
        # ASAP and ALAP schedules
        t = asap_scheduling(dfg, delta)
        b = alap_scheduling(dfg, delta)

        # fix already scheduled t and b values
        for op in 1:nv(dfg)
            if (phi[op] > 0)
                t[op] = phi[op]
                b[op] = phi[op]
            end
        end


        ## Step 2
        # probability distribution
        dim = max(maximum(t), maximum(b))
        prob = zeros(AbstractFloat, (nv(dfg), dim))
        for op in 1:nv(dfg)
            for i in 1:dim
                if (t[op] <= i <= b[op])
                    prob[op, i] = 1 / (b[op] - t[op] + 1)
                end
            end
        end

        # Distribution graph
        DG = Dict{Symbol,Vector{AbstractFloat}}()
        for (k, _) in delta
            DG[k] = zeros(AbstractFloat, dim)
            for op in 1:nv(dfg)
                type = get_prop(dfg, op, :type)
                if (type == k)
                    for i in 1:dim
                        DG[k][i] += prob[op, i]
                    end
                end
            end
        end


        ## Step 3
        # SelfForce
        self_force = zeros(AbstractFloat, (nv(dfg), dim))
        for op in 1:nv(dfg)
            type = get_prop(dfg, op, :type)

            part = 0.0
            for l in t[op]:b[op]
                part -= DG[type][l] / (b[op] - t[op] + 1)
            end

            for i in 1:dim
                self_force[op, i] = part + DG[type][i]
            end
        end


        ## Step 4
        # TODO  calculate force

        # TODO calculate pred force
        pred_force = zeros(AbstractFloat, (nv(dfg), dim))

        # TODO calculate succ force
        succ_force = zeros(AbstractFloat, (nv(dfg), dim))


        ## Step 5
        total_force = zeros(AbstractFloat, (nv(dfg), dim))
        for op in 1:nv(dfg)
            for i in 1:dim
                total_force[op, i] = self_force[op, i]
                total_force[op, i] += pred_force[op, i]
                total_force[op, i] += succ_force[op, i]
            end
        end


        ## Step 6
        # Schedule valid operation at clock cycle with minimum total force
        for op in 1:nv(dfg)
            if (phi[op] > 0)
                for i in 1:dim
                    total_force[op, i] = floatmax()
                end
            end
        end
        (_, id) = findmin(total_force)
        phi[id[1]] = id[2]


        # check, if all operations are scheduled
        done = true
        for i in 1:length(phi)
            if (phi[i] == 0)
                done = false
            end
        end
    end

    return phi
end
