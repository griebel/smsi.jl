"""
    general_list_scheduling(
        dfg::MetaDiGraph,
        fe::Dict{Symbol,T},
        pf::Vector{T},
    ) -> Vector{T} where {T<:Integer}

General List Scheduling

Priority function: mobility

# Arguments
- `dfg::MetaDiGraph`: graph of operations including `:type`
- `fe::Dict{Symbol,T}`: number of functional elements per `:type`
- `pf::Vector{T}`: priority function (mobility)

# Returns
- `Vector{T}`: scheduled clock cycle
"""
function general_list_scheduling(
    dfg::MetaDiGraph,
    fe::Dict{Symbol,T},
    pf::Vector{T},
)::Vector{T} where {T<:Integer}
    # get in degree
    in_degree = zeros(T, nv(dfg))
    for i in 1:nv(dfg)
        in_degree[i] = indegree(dfg, i)
    end

    # clock cycle
    c = 1

    # All vertices to iterate over (1 => not yet scheduled)
    V = ones(T, nv(dfg))

    # scheduled clock cycle
    phi = ones(T, nv(dfg))

    # operations
    operations = copy(fe)

    # iterate until all vertices are scheduled
    while sum(V) > 0
        # ready to schedule vertices
        V_ready = zeros(T, nv(dfg))
        for o_i in 1:length(V)
            if ((V[o_i] == 1) && (in_degree[o_i] == 0))
                V_ready[o_i] = 1
            end
        end

        # Sort descending order
        V_sort_ready = Vector{T}()
        max = maximum(pf)
        for i in max:-1:1
            for o_i in 1:nv(dfg)
                if ((V_ready[o_i] == 1) && (pf[o_i] == i))
                    append!(V_sort_ready, o_i)
                end
            end
        end

        # Schedule next operations according to ressources
        V_sched = Vector{T}()
        for o_i in V_sort_ready
            type = get_prop(dfg, o_i, :type)
            if (operations[type] > 0)
                append!(V_sched, o_i)
                V_ready[o_i] = 0
                operations[type] -= 1
            end
        end

        # Assign  clock cycle to
        for o_i in V_sched
            phi[o_i] = c
        end

        # check for ready vertices
        for o_i in V_sched
            V[o_i] = 0

            type = get_prop(dfg, o_i, :type)
            operations[type] += 1

            for o_j in outneighbors(dfg, o_i)
                in_degree[o_j] -= 1
                if (in_degree[o_j] == 0)
                    V_ready[o_j] = 1
                end
            end
        end

        c += 1
    end

    return phi
end
