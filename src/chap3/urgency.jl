"""
    urgency(dfg::MetaDiGraph, delta::Dict{Symbol,T}) -> Vector{T} where {T<:Integer}

Priority function for urgency yield

# Arguments
- `dfg::MetaDiGraph`: graph of operations including `:type`
- `delta::Dict{Symbol,T}`: delay per operation `:type`

# Returns
- `Vector{T}`: priorities according to operation
"""
function urgency(dfg::MetaDiGraph, delta::Dict{Symbol,T})::Vector{T} where {T<:Integer}
    # reverse data flow graph
    rev_dfg = MetaDiGraph(nv(dfg))
    for o_i in 1:nv(dfg)
        ps = props(dfg, o_i)
        set_props!(rev_dfg, o_i, ps)
        for o_j in outneighbors(dfg, o_i)
            add_edge!(rev_dfg, o_j, o_i)
        end
    end

    # ASAP scheduling
    priority = asap_scheduling(rev_dfg, delta)

    return priority
end
