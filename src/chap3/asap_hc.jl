"""
    asap_hc_scheduling(
        dfg::MetaDiGraph,
        delta::Dict{Symbol,T},
        tau::Dict{Symbol,T},
    ) -> Vector{T} where {T<:Integer}

ASAP Scheduling with hardware constraints

# Arguments
- `dfg::MetaDiGraph`: graph of operations including `:type`
- `delta::Dict{Symbol,T}`: delay per operation `:type`
- `tau::Dict{Symbol,T}`: number of available hardware units per `:type`

# Returns
- `Vector{T}`: clock cycle of execution of each operation
"""
function asap_hc_scheduling(
    dfg::MetaDiGraph,
    delta::Dict{Symbol,T},
    tau::Dict{Symbol,T},
)::Vector{T} where {T<:Integer}
    # ressources for worst case (all operations)
    dim = 0
    for o_i in 1:nv(dfg)
        dim += delta[get_prop(dfg, o_i, :type)]
    end
    hardware = Vector{Dict{Symbol,T}}(undef, dim)
    for i in 1:dim
        hardware[i] = copy(tau)
    end

    # get in degree
    in_degree = zeros(T, nv(dfg))
    for i in 1:nv(dfg)
        in_degree[i] = indegree(dfg, i)
    end

    # Define source set
    stack = Stack{T}()
    for i in 1:nv(dfg)
        if (in_degree[i] == 0)
            push!(stack, i)
        end
    end

    # set outputs
    phi = ones(T, nv(dfg))

    # check resources of initial set
    for o_i in stack
        type = get_prop(dfg, o_i, :type)
        while (hardware[phi[o_i]][type] == 0)
            phi[o_i] += 1
        end
        for i in phi[o_i]:phi[o_i]+delta[type]-1
            hardware[i][type] -= 1
        end
    end

    # iterate over all elements until none is left
    while !isempty(stack)
        o_i = pop!(stack)

        # check succeeding operations
        for o_j in outneighbors(dfg, o_i)
            type = get_prop(dfg, o_i, :type)

            # derive delay
            phi[o_j] = max(phi[o_i] + delta[type], phi[o_j])

            # remove input
            in_degree[o_j] -= 1

            # if no inputs left, add to stack
            if (in_degree[o_j] == 0)
                # add to stack
                push!(stack, o_j)

                # update hardware
                for i in phi[o_j]-delta[type]+1:phi[o_j]
                    hardware[i][type] -= 1
                end
            end
        end
    end

    return phi
end
