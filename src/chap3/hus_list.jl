"""
    hus_list_scheduling(
        dfg::SimpleDiGraph,
        fe::T,
        pf::Vector{T}
    ) -> Vector{T} where {T<:Integer}

Hu's List Scheduling

Priority function: mobility

# Arguments
- `dfg::SimpleDiGraph`: graph of operations
- `fe::T`: number of functional elements
- `pf::Vector{T}`: priority function (mobility)

# Returns
- `Vector{T}`: scheduled clock cycle
"""
function hus_list_scheduling(
    dfg::SimpleDiGraph,
    fe::T,
    pf::Vector{T}
)::Vector{T} where {T<:Integer}
    @assert 0 < fe "Variable needs to be within: 0 < fe"

    # get in degree
    in_degree = zeros(T, nv(dfg))
    for i in 1:nv(dfg)
        in_degree[i] = indegree(dfg, i)
    end

    # clock cycle
    c = 1

    # All vertices to iterate over (1 => not yet scheduled)
    V = ones(T, nv(dfg))

    # ready to schedule vertices
    V_ready = zeros(T, nv(dfg))
    for o_i in 1:nv(dfg)
        if (in_degree[o_i] == 0)
            V_ready[o_i] = 1
        end
    end

    # scheduled clock cycle
    phi = ones(T, nv(dfg))

    # iterate until all vertices are scheduled
    while sum(V) > 0
        # Sort descending order
        V_sort_ready = Vector{T}()
        max = maximum(pf)
        for i in max:-1:1
            for o_i in 1:nv(dfg)
                if ((V_ready[o_i] == 1) && (pf[o_i] == i))
                    append!(V_sort_ready, o_i)
                end
            end
        end

        # Schedule next operations according to ressources
        V_sched = Vector{T}()
        for i in 1:fe
            if i > length(V_sort_ready)
                break
            end

            append!(V_sched, V_sort_ready[i])
            V_ready[V_sort_ready[i]] = 0
        end

        # Assign  clock cycle to
        for o_i in V_sched
            phi[o_i] = c
        end

        # check for ready vertices
        for o_i in V_sched
            V[o_i] = 0
            for o_j in outneighbors(dfg, o_i)
                in_degree[o_j] -= 1
                if (in_degree[o_j] == 0)
                    V_ready[o_j] = 1
                end
            end
        end

        # increase clcok cycle count
        c += 1
    end

    return phi
end
