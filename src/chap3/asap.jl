"""
    asap_scheduling(
        dfg::MetaDiGraph,
        delta::Dict{Symbol,T}
    ) -> Vector{T} where {T<:Integer}

ASAP Scheduling

# Arguments
- `dfg::MetaDiGraph`: graph of operations including `:type`
- `delta::Dict{Symbol,T}`: delay per operation `:type`

# Returns
- `Vector{T}`: clock cycle of execution of each operation
"""
function asap_scheduling(
    dfg::MetaDiGraph,
    delta::Dict{Symbol,T}
)::Vector{T} where {T<:Integer}
    # get in degree
    in_degree = zeros(T, nv(dfg))
    for i in 1:nv(dfg)
        in_degree[i] = indegree(dfg, i)
    end

    # Define source set
    stack = Stack{T}()
    for i in 1:nv(dfg)
        if (in_degree[i] == 0)
            push!(stack, i)
        end
    end

    # set outputs
    phi = ones(T, nv(dfg))
    while (!isempty(stack))
        o_i = pop!(stack)
        for o_j in outneighbors(dfg, o_i)
            type = get_prop(dfg, o_i, :type)
            phi[o_j] = max(phi[o_i] + delta[type], phi[o_j])
            in_degree[o_j] -= 1
            if (in_degree[o_j] == 0)
                push!(stack, o_j)
            end
        end
    end

    return phi
end
