module SMSI

using DataStructures
using LightGraphs
using MetaGraphs
using Random

# Shared functions
include("common/path.jl")

# Chapter 2
############################################################################################
include("chap2/amdahls_law.jl")
export amdahl_fraction, amdahl_speedup

include("chap2/gustafsons_law.jl")
export gustafson_speedup

include("chap2/kernighan_lin.jl")
export kernighan_lin

include("chap2/simulated_annealing.jl")
export simulated_annealing

include("chap2/threshold_acceptance.jl")
export threshold_acceptance

include("chap2/ant_colony_optimization.jl")
export ant_colony_optimization

# Chapter 3
############################################################################################
include("chap3/asap.jl")
export asap_scheduling

include("chap3/asap_hc.jl")
export asap_hc_scheduling

include("chap3/alap.jl")
export alap_scheduling

include("chap3/mobility.jl")
export mobility

include("chap3/urgency.jl")
export urgency

include("chap3/hus_list.jl")
export hus_list_scheduling

include("chap3/general_list.jl")
export general_list_scheduling

include("chap3/advanced_general_list.jl")
export advanced_general_list_scheduling

include("chap3/force_directed.jl")
export force_directed_scheduling

end
