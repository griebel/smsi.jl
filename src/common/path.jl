"""
    distance(
        from::Tuple{T,T},
        to::Tuple{T,T}
    )::AbstractFloat where {T<:Integer}

Calculates the distance between two coordinates.

# Arguments
- `from::Tuple{T,T}`: first coordinate
- `to::Tuple{T,T}`: second coorinate

# Returns
- `AbstractFloat`: distance between coordinates
"""
function distance(
    from::Tuple{T,T},
    to::Tuple{T,T}
)::AbstractFloat where {T<:Integer}
    x_dist = abs(from[1] - to[1])
    y_dist = abs(from[2] - to[2])
    dist = sqrt(x_dist * x_dist + y_dist * y_dist)

    return dist
end


"""
    distance(
        path::Vector{Tuple{T,T}}
    )::AbstractFloat where {T<:Integer}

Calculates the total distance between the coordinates from top to bottom in a closed loop

# Arguments
- `path::Vector{Tuple{T,T}}`: locations in order to be reached

# Returns
- `AbstractFloat`: total distance
"""
function distance(
    path::Vector{Tuple{T,T}}
)::AbstractFloat where {T<:Integer}
    dist = 0
    for i in 1:length(path)-1
        dist += distance(path[i], path[i+1])
    end
    dist += distance(path[end], path[1])

    return dist
end
export distance


"""
    random_exchange_pairs(
        path::Vector{Tuple{T,T}}
    )::Vector{Tuple{T,T}} where {T<:Integer}

Exchange a random pair of coordinates in the path

# Arguments
- `Vector{Tuple{T,T}}`: current path

# Returns
- `Vector{Tuple{T,T}}`: modified path
"""
function random_exchange_pairs(
    path::Vector{Tuple{T,T}}
)::Vector{Tuple{T,T}} where {T<:Integer}
    val1 = Int(floor(rand() * (length(path) - 1)) + 1)
    val2 = Int(ceil(rand() * (length(path) - 1)) + 1)

    new_path = copy(path)
    new_path[val1] = path[val2]
    new_path[val2] = path[val1]
    return new_path
end
