@testset "Gustafson's Law" begin
    @test 1 == 1

    # Define constants
    t_s = 3000

    # Define scalables
    t_p = 5000
    N = 4

    # Check speedup
    S = gustafson_speedup(t_s, t_p, N)
    @test S == 1.8823529411764706
end
