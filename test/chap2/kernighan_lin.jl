@testset "Kernighan Lin" begin
    @test 1 == 1

    # Define graph
    S = SimpleGraph(4)
    add_edge!(S, 1, 2)
    add_edge!(S, 1, 3)
    add_edge!(S, 1, 4)
    add_edge!(S, 2, 3)

    # Define initial partitions
    A_0 = [1, 2]
    B_0 = [3, 4]

    # Define initial cut size
    c_0 = 3

    # Algorithm iteration 1
    (c_1, A_1, B_1) = kernighan_lin(S, c_0, A_0, B_0)
    @test c_1 == 2
    @test A_1 == [3, 2]
    @test B_1 == [1, 4]

    # Algorithm iteration 2
    (c_2, A_2, B_2) = kernighan_lin(S, c_1, A_1, B_1)
    @test c_2 == 2
    @test A_2 == [3, 2]
    @test B_2 == [1, 4]
end
