@testset "Threshold Acceptance" begin
    @test 1 == 1

    # Random path
    path = [
        (200, 40),
        (60, 80),
        (100, 40),
        (180, 60),
        (20, 20),
        (180, 200),
        (120, 80),
        (60, 200),
        (20, 40),
        (200, 160),
        (140, 140),
        (60, 20),
        (160, 20),
        (140, 180),
        (180, 100),
        (20, 160),
        (80, 180),
        (40, 120),
    ]

    # Test algorithm
    opt_cost, opt_path = threshold_acceptance(path, 0.01, 2500)
    @test opt_path != path
    @test opt_cost < SMSI.distance(path)
end
