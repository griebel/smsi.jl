@testset "Amdahl's Law" begin
    @test 1 == 1

    # Define constants
    t_s = 3000
    t_p = 5000

    # Check fraction result
    f = amdahl_fraction(t_s, t_p)
    @test f == 0.375

    # Define number of processor cores
    N = 4

    # Check speedup
    S = amdahl_speedup(f, N)
    @test S == 1.8823529411764706
end
