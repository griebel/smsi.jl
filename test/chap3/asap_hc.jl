@testset "ASAP HC Scheduling" begin
    @test 1 == 1

    # Create graph
    dfg = MetaDiGraph(6, 1)
    set_props!(dfg, 1, Dict(:type => :multiplier))
    set_props!(dfg, 2, Dict(:type => :adder))
    set_props!(dfg, 3, Dict(:type => :adder))
    set_props!(dfg, 4, Dict(:type => :adder))
    set_props!(dfg, 5, Dict(:type => :multiplier))
    set_props!(dfg, 6, Dict(:type => :adder))
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)
    add_edge!(dfg, 6, 5)

    delta = Dict(:adder => 1, :multiplier => 2)
    tau = Dict(:adder => 1, :multiplier => 1)

    # Test algorithm
    schedule = asap_hc_scheduling(dfg, delta, tau)
    @test schedule == [1, 3, 2, 4, 5, 1]
end
