@testset "General List Scheduling" begin
    @test 1 == 1

    # Create graph
    dfg = MetaDiGraph(5, 1)
    set_props!(dfg, 1, Dict(:type => :multiplier))
    set_props!(dfg, 2, Dict(:type => :adder))
    set_props!(dfg, 3, Dict(:type => :adder))
    set_props!(dfg, 4, Dict(:type => :adder))
    set_props!(dfg, 5, Dict(:type => :multiplier))
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)

    functional_elements = Dict(:adder => 1, :multiplier => 1)
    priority = [3, 3, 2, 2, 1]

    # Test algorithm
    schedule = general_list_scheduling(dfg, functional_elements, priority)
    @test schedule == [1, 1, 2, 3, 4]
end
