@testset "Hu's List Scheduling" begin
    @test 1 == 1

    # Create graph
    dfg = SimpleDiGraph(5)
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)

    priority = [3, 3, 2, 2, 1]
    functional_elements = 2

    # Test algorithm
    schedule = hus_list_scheduling(dfg, functional_elements, priority)
    @test schedule == [1, 1, 2, 2, 3]
end
