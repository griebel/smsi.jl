using LightGraphs
using MetaGraphs
using SMSI
using Test

@testset "SMSI.jl" begin
    include("chap2/amdahls_law.jl")
    include("chap2/gustafsons_law.jl")
    include("chap2/kernighan_lin.jl")
    include("chap2/simulated_annealing.jl")
    include("chap2/threshold_acceptance.jl")
    include("chap2/ant_colony_optimization.jl")

    include("chap3/asap.jl")
    include("chap3/asap_hc.jl")
    include("chap3/alap.jl")
    include("chap3/mobility.jl")
    include("chap3/urgency.jl")
    include("chap3/hus_list.jl")
    include("chap3/general_list.jl")
    include("chap3/advanced_general_list.jl")
    include("chap3/force_directed.jl")
end
