[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://griebel.gitlab.io/smsi.jl/dev/)
[![Build Status](https://gitlab.com/griebel/SMSI.jl/badges/master/pipeline.svg)](https://gitlab.com/griebel/SMSI.jl/pipelines)
[![Coverage](https://gitlab.com/griebel/SMSI.jl/badges/master/coverage.svg)](https://gitlab.com/griebel/SMSI.jl/commits/master)

# SMSI

The in the SMSI lecture introduced algorithms are shown here with very basic implementations.

## Examples

Notebooks with examples can be found [here](https://gitlab.com/griebel/smsi-examples)